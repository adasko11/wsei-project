import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserComponent } from './user/user.component';
import { LogoComponent } from './global-components/logo/logo.component';
import { ReadonlyprofileComponent } from './user/readonlyprofile/readonlyprofile.component';
import { ShowProfileEngineComponent } from './user/readonlyprofile/show-profile-engine/show-profile-engine.component';
import { ShowPhotoComponent } from './user/readonlyprofile/show-photo/show-photo.component';
import { ShowNameComponent } from './user/readonlyprofile/show-name/show-name.component';
import { ShowOpinionComponent } from './user/readonlyprofile/show-opinion/show-opinion.component';
import { ShowTematicalModuleComponent } from './user/readonlyprofile/show-tematical-module/show-tematical-module.component';
import { EditProfileComponent } from './user/edit-profile/edit-profile.component';
import { EditProfileEngineComponent } from './user/edit-profile/edit-profile-engine/edit-profile-engine.component';
import { TextFieldComponent } from './user/edit-profile/text-field/text-field.component';
import { AddButtonComponent } from './user/edit-profile/add-button/add-button.component';
import { CancleButtonComponent } from './user/edit-profile/cancle-button/cancle-button.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    LogoComponent,
    ReadonlyprofileComponent,
    ShowProfileEngineComponent,
    ShowPhotoComponent,
    ShowNameComponent,
    ShowOpinionComponent,
    ShowTematicalModuleComponent,
    EditProfileComponent,
    EditProfileEngineComponent,
    TextFieldComponent,
    AddButtonComponent,
    CancleButtonComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
