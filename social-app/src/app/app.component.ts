import { Component, NgModule } from '@angular/core';
import { UserComponent } from './user/user.component';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
@NgModule({
  declarations:[
    UserComponent
  ],
})
export class AppComponent {
  title = 'social-app';
}
